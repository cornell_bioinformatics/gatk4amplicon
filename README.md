# gatk4amplicon.py

This python script (gatk4amplicon.py) is developed for the Vitisgen2 project (https://www.vitisgen2.org/) to analyzes multi-plexed amplicon sequencing data using GATK.

## Getting Started


### Prerequisites
1. Python 3 (tested on python 3.6)  
2. Other software.  
The following commands should be installed and in the PATH:  
 * bwa: http://bio-bwa.sourceforge.net/  
 * samtools: http://samtools.sourceforge.net/  
 * gatk 4: https://software.broadinstitute.org/gatk/  

### Installation
Download the script gatk4amplicon.py and put it in any directory

### Usage
1. Preparing reference database.  
  You will need a reference sequence database. You can need a fasta file with either one of the following:  
  * The reference genome fasta  
  * A fasta file with amplicon sequence template  
  * You can also run our amplicon.py script (https://bitbucket.org/cornell_bioinformatics/amplicon), which will output a fasta file with top allele per marker  
  
2. Index the reference fasta file  
gatk CreateSequenceDictionary -R reference.fa  -O reference.dict  
samtools faidx reference.fa  
bwa index reference.fa  
  
3. Prepare a tab-delimited text file for the samples.  
A tab-delimited text file with three columns. 1)Sample Name; 2) Paired-end sequence file 1 (fastq or fastq.gz); 3) Paired-end sequence file 2.  

4. Run the command within a directory will all the fastq data files
a. prepare the reference:
gatk_genome.sh input.fasta refDirectroy

b. run gatk
gatk4amplicon.py -s sample.txt -r refDirectroy -o outDirectoryName -j 10 -t 4    

  -h, --help            show this help message and exit
  -s SAMPLE, --sample SAMPLE
                        Sample file. Tab delimited text file with 3 or 4
                        columns: sample_Name, plate_well, fastq_file1,
                        (optional)fastq_file2. Plate_well is a string to
                        uniquely define sample if sample names are duplicated.
  -o OUTPUT, --output OUTPUT
                        Output directory
  -r REFERENCE, --reference REFERENCE
                        directory of reference db. the directory should be
                        prepared with the script
                        /home/vitisgen/tools/gatk_genome.sh ref.fasta
                        genomeDir
  -m TRIM, --trim TRIM  Trim both the 5- and 3-prime end of the amplicon by
                        this length. Default 25. Set to 0 for no trimming. The
                        is necessary as primer regions are not reliable for
                        genotyping.
  -j JOB, --job JOB     Number of simultaneous jobs. Default:8
  -t THREAD, --thread THREAD
                        Number of threads per job. Default:1
  -d MERGEDUPLICATE, --mergeDuplicate MERGEDUPLICATE
                        Whether to merge the duplicate samples. 1: merge; 0:
                        not merge and the duplicated sample will be named
                        <sampleName>__<index starting from 1> . Default:1
  -x SLURMCLUSTER, --slurmcluster SLURMCLUSTER
                        Slurm cluster name. Slurm is configured to run on
                        Cornell BioHPC. e.g. "-x cbsumm10"
  -y SLURMBATCHSIZE, --slurmBatchSize SLURMBATCHSIZE
                        Slurm job size. Number of samples per slurm job.
                        default 10
  -i SKIP, --skip SKIP  Skip steps. e.g. "-i 1" to skip steps 1. the steps
                        are: 1. haplotype caller; 2. genotype

  
### Output files
  * all.vcf


## Authors
* **Qi Sun**

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments
